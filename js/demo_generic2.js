function useState() {
    let state;
    function getState() {
        return state;
    }
    function setState(x) {
        state = x;
    }
    return { getState, setState };
}
let numberUseState = useState();
numberUseState.setState(2);
// numberUseState.setState("20");
let stringUstate = useState();
stringUstate.setState("Hello Alice");
// let booleanState = useState<boolean>(); err
// default string useState value
let defaultUstate = useState();
defaultUstate.setState("hello Alice");
// let defaultUstate = useState<number>();
// defaultUstate.setState(2022);
function objectUseState() {
    let objectValue;
    function getObjectState() {
        return objectValue;
    }
    function setObjectState(x, y) {
        objectValue = {
            firstKey: x,
            seconKey: y,
        };
    }
    return { getObjectState, setObjectState };
}
let { getObjectState, setObjectState } = objectUseState();
setObjectState("Alice", 2);
console.log(getObjectState());
//# sourceMappingURL=demo_generic2.js.map