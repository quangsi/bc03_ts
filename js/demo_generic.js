let numArr1 = [1, 2, 3];
let numArr2 = [2, 3, 2];
function useState() {
    let state;
    function getState() {
        return state;
    }
    function setState(x) {
        state = x;
    }
    return { getState, setState };
}
// let { getState, setState } = useState();
// setState("50");
// setState(50);
// console.log(getState());
let num_string_useState = useState();
num_string_useState.setState(100);
num_string_useState.setState("100");
console.log(num_string_useState.getState());
//# sourceMappingURL=demo_generic.js.map