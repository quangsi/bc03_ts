console.log("hello");
// basis type  ~ primitive value ~ pass by value
// variable:type
let userAge = 2;
userAge = 10;
// userAge="10"
let username = "Bob";
let isHoliday = true;
let isMarried = null;
// isMarried=true
let is_married = undefined;
let someValue = "yes";
someValue = 1;
someValue = true;
someValue = null;
//   desc?: string;  optional property
let todo1 = {
    id: 1,
    content: "làm dự án cuối khoá",
    isCompleted: true,
};
let todo2 = {
    id: 1,
    content: "ăn cơm",
    isCompleted: false,
    alertTime: "today",
};
let per1 = { id: 231, age: 2, name: "tom" };
// class (Prototype) :Dùng để định dạng dữ liệu, tạo tạo đối tượng (instance ) từ class đó
class SinhVien {
    constructor() {
        this.id = 0;
        this.name = "";
    }
}
let sv1 = { id: 12, name: "bob" };
let sv2 = new SinhVien();
// Array
let arrNumber = [1, 2, 4];
// generic
let arrId = [2, 4, 5];
let todoList = [
    { id: 1, content: "home work", isCompleted: false },
    { id: 2, content: "house work", isCompleted: false },
];
// tuple
let sv = [1, "tom"];
console.log(sv[0]);
// 1
console.log(sv[1]);
// tom
//  nội suy
let account = "alice123";
// account = 2;
// function
let tinhTong = (a, b) => {
    return a + b;
};
let sayHello = () => {
    console.log("hello");
    //   return "hello";
};
// let introduce = (
//   callback: (name: string)  =>  {
//     console.log("yes");
//   }
// ) => {
// };
//
let arrTitle = ["tom", "mun", "Alice"];
let arrList = [1, 2, 3, 5];
// advanced type
let user = null;
let age = 2;
// enum : kiểu dữ liệu liệt kê, quy định giá trị của loại dữ liệu
var ActiveType;
(function (ActiveType) {
    ActiveType[ActiveType["active"] = 1] = "active";
    ActiveType[ActiveType["noneActive"] = 0] = "noneActive";
})(ActiveType || (ActiveType = {}));
let buttonEl = {
    id: 1,
    name: "Music",
    active: ActiveType.active,
};
//  any, unkown : chấp nhận tất cả các giá trị của bất kì dữ liệu nào. Lưu ý không nên lạm dụng any, unkown
let value1 = 1;
value1 = "yes";
// any : có thể gán giá trị cho biến khác
let value2 = "Hello";
// unknow : chỉ có thể lưu và ko thể gán giá trị cho biến khác
value2 = true;
let value3 = value1;
// Partial => requried to optional
let introduceCar = (value) => {
    console.log(value.id);
};
introduceCar({
    name: "VinSedan",
});
// introduceProduct({
//   id: 1,
//   name: "SS1",
//   price: 2,
//   decs: "Tốt",
// });
let myProduct = {
    id: 1,
    name: "Iphone xssss 12",
    price: 2,
};
// myProduct.name = "Samsung 12";
//# sourceMappingURL=index.js.map