function useState<T extends number | string = string>() {
  let state: T;
  function getState() {
    return state;
  }
  function setState(x: T) {
    state = x;
  }
  return { getState, setState };
}

let numberUseState = useState<number>();
numberUseState.setState(2);
// numberUseState.setState("20");
let stringUstate = useState<string>();
stringUstate.setState("Hello Alice");
// let booleanState = useState<boolean>(); err

// default string useState value
let defaultUstate = useState();
defaultUstate.setState("hello Alice");
// let defaultUstate = useState<number>();
// defaultUstate.setState(2022);

// function objectUseState<
//   F extends string | number = number,
//   S extends number | string = string
// >()
function objectUseState<F extends string | boolean, S extends number | F>() {
  let objectValue: {
    firstKey: F;
    seconKey: S;
  };

  function getObjectState() {
    return objectValue;
  }

  function setObjectState(x: F, y: S) {
    objectValue = {
      firstKey: x,
      seconKey: y,
    };
  }
  return { getObjectState, setObjectState };
}

let { getObjectState, setObjectState } = objectUseState<string, string>();

setObjectState("Alice", "2");

console.log(getObjectState());
