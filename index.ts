console.log("hello");

// basis type  ~ primitive value ~ pass by value

// variable:type

let userAge: number = 2;
userAge = 10;

// userAge="10"

let username: string = "Bob";
let isHoliday: boolean = true;
let isMarried: null = null;
// isMarried=true
let is_married: undefined = undefined;

let someValue: any = "yes";
someValue = 1;
someValue = true;
someValue = null;

// preferences value ~ pass by reference ( object , array,..)

// interface dùng để định dạng format của object, interface sẽ được remove sau khi build

interface Todo {
  id: number;
  content: string;
  isCompleted: boolean;
  desc?: string;
}

interface NewTodo extends Todo {
  alertTime: string;
}
//   desc?: string;  optional property

let todo1: Todo = {
  id: 1,
  content: "làm dự án cuối khoá",
  isCompleted: true,
};
let todo2: NewTodo = {
  id: 1,
  content: "ăn cơm",
  isCompleted: false,
  alertTime: "today",
};

// type : mô tả format object

type Person = {
  id: number;
  name: string;
  age: number;
  profile?: string;
};

let per1 = { id: 231, age: 2, name: "tom" };

// class (Prototype) :Dùng để định dạng dữ liệu, tạo tạo đối tượng (instance ) từ class đó
class SinhVien {
  id: number = 0;

  name: string = "";
  constructor() {}
}

let sv1: SinhVien = { id: 12, name: "bob" };

let sv2 = new SinhVien();

// Array

let arrNumber: number[] = [1, 2, 4];

// generic

let arrId: Array<number> = [2, 4, 5];

let todoList: Todo[] = [
  { id: 1, content: "home work", isCompleted: false },
  { id: 2, content: "house work", isCompleted: false },
];
// tuple
let sv = [1, "tom"];
console.log(sv[0]);
// 1
console.log(sv[1]);
// tom

//  nội suy
let account = "alice123";

// account = 2;

// function

let tinhTong = (a: number, b: number): number => {
  return a + b;
};
let sayHello = (): void => {
  console.log("hello");
  //   return "hello";
};
// let introduce = (
//   callback: (name: string)  =>  {

//     console.log("yes");

//   }
// ) => {

// };

//

let arrTitle: Array<string> = ["tom", "mun", "Alice"];

interface List<T> {
  length: number;
  [index: number]: T;
}
let arrList: List<number> = [1, 2, 3, 5];
// advanced type

let user: {} | null = null;
// union type : quy định loại dữ liệu
type ResponseAgeBE = number | string;
let age: ResponseAgeBE = 2;
// enum : kiểu dữ liệu liệt kê, quy định giá trị của loại dữ liệu

enum ActiveType {
  active = 1,
  noneActive = 0,
}

interface ButtonMenu {
  id: number | string;
  name: string;
  active: ActiveType;
}

let buttonEl: ButtonMenu = {
  id: 1,
  name: "Music",
  active: ActiveType.active,
};
//  any, unkown : chấp nhận tất cả các giá trị của bất kì dữ liệu nào. Lưu ý không nên lạm dụng any, unkown

let value1: any = 1;
value1 = "yes";
// any : có thể gán giá trị cho biến khác

let value2: unknown = "Hello";
// unknow : chỉ có thể lưu và ko thể gán giá trị cho biến khác
value2 = true;

let value3: string = value1;
// let value4: string = value2;

interface Car {
  id: number;
  name: string;
  price: number;
  desc: string;
}

// Partial => requried to optional
let introduceCar = (value: Partial<Car>) => {
  console.log(value.id);
};
introduceCar({
  name: "VinSedan",
});

// Required : optional to required

interface Product {
  id: number;
  name: string;
  price: number;
  decs?: string;
}
// introduceProduct({
//   id: 1,
//   name: "SS1",
//   price: 2,
//   decs: "Tốt",
// });

let myProduct: Readonly<Product> = {
  id: 1,
  name: "Iphone xssss 12",
  price: 2,
};

// myProduct.name = "Samsung 12";
