let numArr1: number[] = [1, 2, 3];
let numArr2: Array<number> = [2, 3, 2];
function useStatee() {
  let state: number | string;
  function getState() {
    return state;
  }
  function setState(x: number | string) {
    state = x;
  }
  return { getState, setState };
}
// let { getState, setState } = useStatee();
// setState("50");
// setState(50);

// console.log(getState());

let num_string_useStatee = useStatee();
num_string_useStatee.setState(100);
num_string_useStatee.setState("100");
console.log(num_string_useStatee.getState());
